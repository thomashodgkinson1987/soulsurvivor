using Muffin;
using UnityEngine;
using Zenject;

public class MainInstaller : MonoInstaller
{

    #region Inspector fields

    [SerializeField] private Camera m_camera = default;
    [Space]
    [SerializeField] private GameObject m_actorPrefab = default;
    [SerializeField] private Transform m_actorParent = default;
    [Space]
    [SerializeField] private GameObject m_forceSourcePrefab = default;
    [SerializeField] private Transform m_forceSourceParent = default;

    #endregion // Inspector fields



    #region MonoInstaller overrides

    public override void InstallBindings ()
    {
        Bind_Logger();
        Bind_Factories();
        Bind_Repositories();
        Bind_Controllers();
        Bind_App();
    }

    #endregion // MonoInstaller overrides



    #region Bind methods

    private void Bind_Logger ()
    {
        Container.Bind<Muffin.ILogger>().To<UnityDebugLogger>().AsSingle();
    }

    private void Bind_Factories ()
    {
        // Camera controller model factory
        {
            Container.Bind<ICameraControllerModelFactory>().To<CameraControllerModelFactory>().AsSingle();
        }

        // Actor model Factory
        {
            Container.Bind<IActorModelFactory>().To<ActorModelFactory>().AsSingle();
        }

        // Actor factory
        {
            Container.Bind<IActorFactory>().To<ActorFactory>().AsSingle()
                .WithArguments(m_actorPrefab, m_actorParent, new ActorModel());
        }


        // Force source model factory
        {
            Container.Bind<IForceSourceModelFactory>().To<ForceSourceModelFactory>().AsSingle();
        }

        // Force source factory
        {
            Container.Bind<IForceSourceFactory>().To<ForceSourceFactory>().AsSingle()
                .WithArguments(m_forceSourcePrefab, m_forceSourceParent);
        }
    }

    private void Bind_Repositories ()
    {
        // Actor model repository
        {
            Container.Bind<IRepository<IActorModel>>().To<Repository<IActorModel>>().AsSingle();
        }

        // Force source model repository
        {
            Container.Bind<IRepository<IForceSourceModel>>().To<Repository<IForceSourceModel>>().AsSingle();
        }
    }

    private void Bind_Controllers ()
    {
        // Camera controller
        {
            Container.Bind<Camera>().FromInstance(m_camera).AsSingle()
                .WhenInjectedInto<ICameraController>();

            Container.Bind<ICameraController>().To<CameraController>().AsSingle();
        }

        // Game controller
        {
            Container.Bind<IGameController>().To<GameController>().AsSingle();
        }
    }

    private void Bind_App ()
    {
        Container.BindInterfacesAndSelfTo<App>().AsSingle();
    }

    #endregion // Bind methods

}
