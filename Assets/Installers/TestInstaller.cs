﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Muffin
{
    public class TestInstaller : MonoInstaller
    {

        #region Public methods

        public override void InstallBindings ()
        {
            RepositoryTests();
        }

        #endregion // Public methods



        #region Private methods

        private void RepositoryTests ()
        {
            IActorModelFactory actorModelFactory = new ActorModelFactory();

            IList<IActorModel> list = new List<IActorModel>();
            for (int i = 0; i < 50; i++)
            {
                IActorModel model = actorModelFactory.Create();
                list.Add(model);
            }

            IDataSource<IActorModel> dataSource = new DataSource<IActorModel>(list);
            IRepository<IActorModel> repository = new Repository<IActorModel>(dataSource);

            Bounds bounds = new Bounds(new Vector3(0f, 0f, 0f), new Vector3(3f, 3f, 0f));
            IFindQuery<IActorModel> findQuery1 = new FindActorsWithinBoundsQuery(bounds);
            IFindQuery<IActorModel> findQuery2 = new FindFromPredicateQuery<IActorModel>(model => model.JumpCount >= 3);
            IFindQuery<IActorModel> findQuery3 = new FindFromPredicateQuery<IActorModel>(model => model.JumpCount < 3);
            IFindQuery<IActorModel> findQuery4 = new FindFromPredicateQuery<IActorModel>(model =>
                model.Position.x >= bounds.min.x && model.Position.x <= bounds.max.x
                && model.Position.y >= bounds.min.y && model.Position.y <= bounds.max.y
            );

            IHybridFindQuery<IActorModel, Vector3> hybridQuery1 = new HybridFindPositionsOfActorsQuery();
            IHybridFindQuery<IActorModel, float> hybridQuery2 = new HybridFindVelocityXOfActorsQuery();
            IHybridFindQuery<IActorModel, Color> hybridQuery3 = new HybridFindPredicateQuery<IActorModel, Color>(model => new Color(model.ColorR, model.ColorG, model.ColorB, model.ColorA));

            Debug.Log("\nFind Query 1: Actors within bounds\n");
            IEnumerable<IActorModel> findQueryResults1 = repository.Find(findQuery1);
            foreach (IActorModel model in findQueryResults1) Debug.Log($"Position: {model.Position.ToString()}");

            Debug.Log("\nFind Query 2: Actors with jump count greater than 2\n");
            IEnumerable<IActorModel> findQueryResults2 = repository.Find(findQuery2);
            foreach (IActorModel model in findQueryResults2) Debug.Log($"Jump Count: {model.JumpCount}");

            Debug.Log("\nFind Query 3: Actors with jump count less than 3\n");
            IEnumerable<IActorModel> findQueryResults3 = repository.Find(findQuery3);
            foreach (IActorModel model in findQueryResults3) Debug.Log($"Jump Count: {model.JumpCount}");

            Debug.Log("\nFind Query 4:(Predicate version) Actors within bounds\n");
            IEnumerable<IActorModel> findQueryResults4 = repository.Find(findQuery4);
            foreach (IActorModel model in findQueryResults4) Debug.Log($"Position: {model.Position.ToString()}");

            Debug.Log("\nHyrbid Query 1: Positions of actors\n");
            IEnumerable<Vector3> hybridQueryResults1 = repository.HybridFind(hybridQuery1);
            foreach (Vector3 position in hybridQueryResults1) Debug.Log($"Position: {position.ToString()}");

            Debug.Log("\nHybrid Query 2: VelocityX of actors\n");
            IEnumerable<float> hybridQueryResults2 = repository.HybridFind(hybridQuery2);
            foreach (float velocityX in hybridQueryResults2) Debug.Log($"VelocityX: {velocityX.ToString()}");

            Debug.Log("\nHybrid Query 3: Color of actors\n");
            IEnumerable<Color> hybridQueryResults3 = repository.HybridFind(hybridQuery3);
            foreach (Color color in hybridQueryResults3) Debug.Log($"Color: R={color.r} G={color.g} B={color.b} A={color.a}");
        }

        #endregion // Private methods

    }
}
