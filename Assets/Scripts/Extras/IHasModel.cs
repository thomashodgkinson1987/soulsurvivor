﻿namespace Muffin
{
    public interface IHasModel<T> : ICanImportModel<T>, ICanExportModel<T>
    {
    }
}
