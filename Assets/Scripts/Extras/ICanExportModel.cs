﻿namespace Muffin
{
    public interface ICanExportModel<T>
    {
        T ExportModel ();
    }
}
