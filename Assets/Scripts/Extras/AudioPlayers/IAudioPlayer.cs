﻿namespace Muffin
{
    public interface IAudioPlayer
    {
        void PlaySound (int id, float volumeScale = 1f);
        void PlayMusic (int id);
        void StopMusic ();
        void StopAll ();
    }
}