﻿using UnityEngine;

namespace Muffin
{
    public class UnityDebugLogger : ILogger
    {

        #region Constructors

        public UnityDebugLogger () { }

        #endregion // Constructors



        #region Public methods

        public void Log (string message)
        {
            Debug.Log(message);
        }

        public void LogWarning (string message)
        {
            Debug.LogWarning(message);
        }

        public void LogError (string message)
        {
            Debug.LogError(message);
        }

        #endregion // Public methods

    }
}
