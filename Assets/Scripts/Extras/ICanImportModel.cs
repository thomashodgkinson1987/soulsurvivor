﻿namespace Muffin
{
    public interface ICanImportModel<T>
    {
        void ImportModel (T model);
    }
}
