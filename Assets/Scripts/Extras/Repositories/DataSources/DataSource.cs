﻿using System.Collections.Generic;

namespace Muffin
{
    public class DataSource<T> : IDataSource<T>
    {

        #region Properties

        public IList<T> Data { get; }

        #endregion // Properties



        #region Constructors

        public DataSource (IList<T> data)
        {
            Data = data;
        }

        #endregion // Constructors

    }
}
