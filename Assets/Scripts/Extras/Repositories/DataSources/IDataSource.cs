﻿using System.Collections.Generic;

namespace Muffin
{
    public interface IDataSource<T>
    {
        IList<T> Data { get; }
    }
}