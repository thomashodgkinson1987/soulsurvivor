﻿using System.Collections.Generic;

namespace Muffin
{
    public interface IReadRepository<T>
    {

        #region Properties

        int Count { get; }

        #endregion // Properties



        #region Methods

        T Get (int id);
        IEnumerable<T> Find (IFindQuery<T> query);
        IEnumerable<T2> HybridFind<T2> (IHybridFindQuery<T, T2> query);

        #endregion // Methods

    }
}
