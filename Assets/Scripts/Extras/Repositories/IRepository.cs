﻿namespace Muffin
{
    public interface IRepository<T> : IReadRepository<T>, IWriteRepository<T> { }
}
