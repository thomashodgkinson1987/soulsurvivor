﻿using System.Collections.Generic;
using System.Linq;

namespace Muffin
{
    public class Repository<T> : IRepository<T>
    {

        #region Fields

        private readonly IDataSource<T> m_dataSource;

        #endregion // Fields



        #region Properties

        public int Count => m_dataSource.Data.Count;

        #endregion // Properties



        #region Constructors

        public Repository (IDataSource<T> dataSource)
        {
            m_dataSource = dataSource;
        }
        public Repository ()
            : this(new DataSource<T>(new List<T>()))
        { }

        #endregion // Constructors



        #region Public methods

        public void Add (T item) => m_dataSource.Data.Add(item);
        public void Remove (T item) => m_dataSource.Data.Remove(item);
        public void Clear () => m_dataSource.Data.Clear();

        public T Get (int id) => m_dataSource.Data.ElementAt(id);
        public IEnumerable<T> Find (IFindQuery<T> query) => query.Query(m_dataSource);
        public IEnumerable<T2> HybridFind<T2> (IHybridFindQuery<T, T2> query) => query.Query(m_dataSource);

        #endregion // Public methods

    }
}
