﻿using System.Collections.Generic;
using System.Linq;

namespace Muffin
{
    public class HybridFindVelocityXOfActorsQuery : IHybridFindQuery<IActorModel, float>
    {

        #region Constructors

        public HybridFindVelocityXOfActorsQuery () { }

        #endregion // Constructors



        #region Public methods

        public IEnumerable<float> Query (IDataSource<IActorModel> dataSource)
        {
            return dataSource.Data.Select(actor => actor.Velocity.x);
        }

        #endregion // Public methods

    }
}
