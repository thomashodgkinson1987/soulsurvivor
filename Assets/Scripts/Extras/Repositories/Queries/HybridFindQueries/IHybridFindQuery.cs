﻿using System.Collections.Generic;

namespace Muffin
{
    public interface IHybridFindQuery<TInput, TOutput>
    {
        IEnumerable<TOutput> Query (IDataSource<TInput> dataSource);
    }
}
