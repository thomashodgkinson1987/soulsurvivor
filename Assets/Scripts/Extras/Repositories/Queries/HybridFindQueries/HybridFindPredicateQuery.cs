﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Muffin
{
    public class HybridFindPredicateQuery<TInput, TOutput> : IHybridFindQuery<TInput, TOutput>
    {

        #region Fields

        private readonly Func<TInput, TOutput> m_predicate;

        #endregion // Fields



        #region Constructors

        public HybridFindPredicateQuery (Func<TInput, TOutput> predicate)
        {
            m_predicate = predicate;
        }

        #endregion // Constructors



        #region Public methods

        public IEnumerable<TOutput> Query (IDataSource<TInput> dataSource)
        {
            return dataSource.Data.Select(m_predicate);
        }

        #endregion // Public methods

    }
}
