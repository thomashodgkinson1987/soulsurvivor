﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Muffin
{
    public class HybridFindPositionsOfActorsQuery : IHybridFindQuery<IActorModel, Vector3>
    {

        #region Constructors

        public HybridFindPositionsOfActorsQuery () { }

        #endregion // Constructors



        #region Public methods

        public IEnumerable<Vector3> Query (IDataSource<IActorModel> dataSource)
        {
            return dataSource.Data.Select(actor => new Vector3(actor.Position.x, actor.Position.y, actor.Position.z));
        }

        #endregion // Public methods

    }
}
