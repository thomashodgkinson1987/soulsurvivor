﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Muffin
{
    public class FindActorsWithinBoundsQuery : IFindQuery<IActorModel>
    {

        #region Fields

        private readonly Bounds m_bounds;

        #endregion // Fields



        #region Constructors

        public FindActorsWithinBoundsQuery (Bounds bounds)
        {
            m_bounds = bounds;
        }

        #endregion // Constructors



        #region IFindQuery methods

        public IEnumerable<IActorModel> Query (IDataSource<IActorModel> dataSource)
        {
            return dataSource.Data.Where(model => m_bounds.Contains(model.Position));
        }

        #endregion // IFindQuery methods

    }
}
