﻿using System.Collections.Generic;

namespace Muffin
{
    public interface IFindQuery<T>
    {
        IEnumerable<T> Query (IDataSource<T> dataSource);
    }
}