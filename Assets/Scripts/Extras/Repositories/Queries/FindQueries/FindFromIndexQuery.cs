﻿using System.Collections.Generic;
using System.Linq;

namespace Muffin
{
    public class FindFromIndexQuery<T> : IFindQuery<T>
    {

        #region Fields

        private readonly int m_index;

        #endregion // Fields



        #region Constructors

        public FindFromIndexQuery (int index)
        {
            m_index = index;
        }

        #endregion // Constructors



        #region Public methods

        public IEnumerable<T> Query (IDataSource<T> dataSource)
        {
            return new T[] { dataSource.Data.ElementAt(m_index) };
        }

        #endregion // Public methods

    }
}
