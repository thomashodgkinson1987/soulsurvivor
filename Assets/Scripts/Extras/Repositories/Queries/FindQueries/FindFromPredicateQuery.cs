﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Muffin
{
    public class FindFromPredicateQuery<T> : IFindQuery<T>
    {

        #region Fields

        private readonly Func<T, bool> m_predigate;

        #endregion // Fields



        #region Constructors

        public FindFromPredicateQuery (Func<T, bool> predicate)
        {
            m_predigate = predicate;
        }

        #endregion // Constructors



        #region Public methods

        public IEnumerable<T> Query (IDataSource<T> dataSource)
        {
            return dataSource.Data.Where(m_predigate);
        }

        #endregion // Public methods

    }
}
