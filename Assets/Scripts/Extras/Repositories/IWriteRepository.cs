﻿namespace Muffin
{
    public interface IWriteRepository<T>
    {
        void Add (T entry);
        void Remove (T entry);
        void Clear ();
    }
}
