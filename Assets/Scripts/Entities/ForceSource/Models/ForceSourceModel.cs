﻿using UnityEngine;

namespace Muffin
{
    public class ForceSourceModel : IForceSourceModel
    {

        #region Properties

        public Vector3 Position { get; set; } = Vector3.zero;
        public float Strength { get; set; } = 5f;
        public float Radius { get; set; } = 3f;

        #endregion // Properties



        #region Constructors

        public ForceSourceModel () { }

        public ForceSourceModel (Vector3 position, float strength, float radius)
        {
            Position = position;
            Strength = strength;
            Radius = radius;
        }

        #endregion // Constructors

    }
}
