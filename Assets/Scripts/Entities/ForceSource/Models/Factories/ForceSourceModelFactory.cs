﻿using UnityEngine;

namespace Muffin
{
    public class ForceSourceModelFactory : IForceSourceModelFactory
    {

        #region Constructors

        public ForceSourceModelFactory () { }

        #endregion // Constructors



        #region Public methods

        public IForceSourceModel Create ()
        {
            return new ForceSourceModel();
        }

        public IForceSourceModel Create (Vector3 position, float strength, float radius)
        {
            return new ForceSourceModel(position, strength, radius);
        }

        #endregion // Public methods

    }
}
