﻿using UnityEngine;

namespace Muffin
{
    public interface IForceSourceModelFactory
    {
        IForceSourceModel Create ();
        IForceSourceModel Create (Vector3 position, float strength, float radius);
    }
}
