﻿using UnityEngine;

namespace Muffin
{
    public interface IForceSourceModel
    {
        Vector3 Position { get; set; }
        float Strength { get; set; }
        float Radius { get; set; }
    }
}
