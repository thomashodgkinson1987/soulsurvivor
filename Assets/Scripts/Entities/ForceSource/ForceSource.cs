﻿using UnityEngine;
using Zenject;

namespace Muffin
{
    [RequireComponent(typeof(CircleCollider2D))]
    public class ForceSource : MonoBehaviour, IHasModel<IForceSourceModel>
    {

        #region Inspector fields

        [SerializeField] private float m_strength = 5f;

        #endregion // Inspector fields



        #region Fields

        private CircleCollider2D m_circleCollider2D;

        // Zenject construct

        private IForceSourceModelFactory m_forceSourceModelFactory;

        #endregion // Fields



        #region Properties

        public float Strength
        {
            get => m_strength;
            private set => m_strength = value;
        }

        public Vector3 Position
        {
            get => transform.position;
            private set => transform.position = value;
        }

        public float Radius
        {
            get => m_circleCollider2D.radius;
            private set => m_circleCollider2D.radius = value;
        }

        #endregion // Properties



        #region Zenject construct

        [Inject]
        private void ZenjectConstruct (IForceSourceModelFactory forceSourceModelFactory)
        {
            m_forceSourceModelFactory = forceSourceModelFactory;
        }

        #endregion // Zenject construct



        #region MonoBehaviour methods

        private void Awake ()
        {
            m_circleCollider2D = GetComponent<CircleCollider2D>();
        }

        #endregion // MonoBehaviour methods



        #region Setters

        public void SetPosition (Vector3 position) => Position = position;
        public void SetStrength (float strength) => Strength = strength;
        public void SetRadius (float radius) => Radius = radius;

        #endregion // Setters



        #region Public methods

        public void ImportModel (IForceSourceModel model)
        {
            SetPosition(model.Position);
            SetStrength(model.Strength);
            SetRadius(model.Radius);
        }

        public IForceSourceModel ExportModel ()
        {
            return m_forceSourceModelFactory.Create(Position, Strength, Radius);
        }

        #endregion // Public methods

    }
}
