﻿using UnityEngine;

namespace Muffin
{
    public interface IForceSourceFactory
    {
        ForceSource Create ();
        ForceSource Create (Vector3 position);
        ForceSource Create (IForceSourceModel model);
    }
}
