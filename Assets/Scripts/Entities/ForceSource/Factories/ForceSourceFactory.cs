﻿using UnityEngine;

namespace Muffin
{
    public class ForceSourceFactory : IForceSourceFactory
    {

        #region Fields

        private readonly GameObject m_prefab;
        private readonly Transform m_parent;

        #endregion // Fields



        #region Constructors

        public ForceSourceFactory (GameObject prefab, Transform parent)
        {
            m_prefab = prefab;
            m_parent = parent;
        }

        #endregion // Constructors



        #region Public methods

        public ForceSource Create ()
        {
            GameObject gameObject = UnityEngine.Object.Instantiate(m_prefab, m_parent);
            gameObject.name = "Force Source";
            ForceSource forceSource = gameObject.GetComponent<ForceSource>();

            return forceSource;
        }

        public ForceSource Create (Vector3 position)
        {
            ForceSource forceSource = Create();
            forceSource.SetPosition(position);

            return forceSource;
        }

        public ForceSource Create (IForceSourceModel model)
        {
            ForceSource forceSource = Create();
            forceSource.SetPosition(model.Position);
            forceSource.SetStrength(model.Strength);
            forceSource.SetRadius(model.Radius);

            return forceSource;
        }

        #endregion // Public methods

    }
}
