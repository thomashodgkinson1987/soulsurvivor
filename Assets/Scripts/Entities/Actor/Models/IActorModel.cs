﻿using UnityEngine;

namespace Muffin
{
    public interface IActorModel
    {
        Vector3 Position { get; set; }
        Vector3 Scale { get; set; }
        Vector3 Rotation { get; set; }

        int Health { get; set; }
        int MinHealth { get; set; }
        int MaxHealth { get; set; }

        float ColorR { get; set; }
        float ColorG { get; set; }
        float ColorB { get; set; }
        float ColorA { get; set; }

        Vector3 Velocity { get; set; }

        float Acceleration { get; set; }
        float Deceleration { get; set; }

        int RayCount { get; set; }
        float Margin { get; set; }

        int JumpCount { get; set; }
        int JumpLimit { get; set; }
        float JumpHeight { get; set; }

        Vector3 GravityModifier { get; set; }

        string[] LeftLayerMaskNames { get; set; }
        string[] RightLayerMaskNames { get; set; }
        string[] DownLayerMaskNames { get; set; }
        string[] UpLayerMaskNames { get; set; }
    }
}
