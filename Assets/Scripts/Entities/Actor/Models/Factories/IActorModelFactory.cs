﻿using UnityEngine;

namespace Muffin
{
    public interface IActorModelFactory
    {
        IActorModel Create ();
        IActorModel Create (IActorModel model);
        IActorModel Create (
            Vector3 position, Vector3 scale, Vector3 rotation,
            int health, int minHealth, int maxHealth,
            float colorR, float colorG, float colorB, float colorA,
            Vector3 velocity,
            float acceleration, float deceleration,
            int rayCount, float margin,
            int jumpCount, int jumpLimit, float jumpHeight,
            Vector3 gravityModifier,
            string[] leftLayerMaskNames, string[] rightLayerMaskNames,
            string[] downLayerMaskNames, string[] upLayerMaskNames);
    }
}
