﻿using UnityEngine;

namespace Muffin
{
    public class ActorModelFactory : IActorModelFactory
    {

        #region Constructors

        public ActorModelFactory () { }

        #endregion // Constructors



        #region Public methods

        public IActorModel Create ()
        {
            return new ActorModel();
        }

        public IActorModel Create (IActorModel model)
        {
            return new ActorModel(
                model.Position, model.Scale, model.Rotation,
                model.Health, model.MinHealth, model.MaxHealth,
                model.ColorR, model.ColorG, model.ColorB, model.ColorA,
                model.Velocity,
                model.Acceleration, model.Deceleration,
                model.RayCount, model.Margin,
                model.JumpCount, model.JumpLimit, model.JumpHeight,
                model.GravityModifier,
                model.LeftLayerMaskNames, model.RightLayerMaskNames,
                model.DownLayerMaskNames, model.UpLayerMaskNames);
        }

        public IActorModel Create (
            Vector3 position, Vector3 scale, Vector3 rotation,
            int health, int minHealth, int maxHealth,
            float colorR, float colorG, float colorB, float colorA,
            Vector3 velocity,
            float acceleration, float deceleration,
            int rayCount, float margin,
            int jumpCount, int jumpLimit, float jumpHeight,
            Vector3 gravityModifier,
            string[] leftLayerMaskNames, string[] rightLayerMaskNames,
            string[] downLayerMaskNames, string[] upLayerMaskNames)
        {
            return new ActorModel(
                position, scale, rotation,
                health, minHealth, maxHealth,
                colorR, colorG, colorB, colorA,
                velocity,
                acceleration, deceleration,
                rayCount, margin,
                jumpCount, jumpLimit, jumpHeight,
                gravityModifier,
                leftLayerMaskNames, rightLayerMaskNames,
                downLayerMaskNames, upLayerMaskNames);
        }

        #endregion // Public methods

    }
}
