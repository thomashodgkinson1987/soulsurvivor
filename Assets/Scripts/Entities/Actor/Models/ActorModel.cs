﻿using System;
using UnityEngine;

namespace Muffin
{
    [Serializable]
    public class ActorModel : IActorModel
    {

        #region Properties

        public Vector3 Position { get; set; } = Vector3.zero;
        public Vector3 Scale { get; set; } = Vector3.one;
        public Vector3 Rotation { get; set; } = Vector3.zero;

        public int Health { get; set; } = 10;
        public int MinHealth { get; set; } = 0;
        public int MaxHealth { get; set; } = 10;

        public float ColorR { get; set; } = 1f;
        public float ColorG { get; set; } = 1f;
        public float ColorB { get; set; } = 1f;
        public float ColorA { get; set; } = 1f;

        public Vector3 Velocity { get; set; } = Vector3.zero;

        public Vector3 GravityModifier { get; set; } = Vector3.one;

        public float Acceleration { get; set; } = 50f;
        public float Deceleration { get; set; } = 100f;

        public int RayCount { get; set; } = 3;
        public float Margin { get; set; } = 0.01f;

        public int JumpCount { get; set; } = 0;
        public int JumpLimit { get; set; } = 2;
        public float JumpHeight { get; set; } = 2.5f;

        public string[] LeftLayerMaskNames { get; set; } = new string[] { "Collision" };
        public string[] RightLayerMaskNames { get; set; } = new string[] { "Collision" };
        public string[] DownLayerMaskNames { get; set; } = new string[] { "Collision" };
        public string[] UpLayerMaskNames { get; set; } = new string[] { "Collision" };

        #endregion // Properties



        #region Constructors

        public ActorModel () { }

        public ActorModel (
            Vector3 position, Vector3 scale, Vector3 rotation,
            int health, int minHealth, int maxHealth,
            float colorR, float colorG, float colorB, float colorA,
            Vector3 velocity,
            float acceleration, float deceleration,
            int rayCount, float margin,
            int jumpCount, int jumpLimit, float jumpHeight,
            Vector3 gravityModifier,
            string[] leftLayerMaskNames, string[] rightLayerMaskNames,
            string[] downLayerMaskNames, string[] upLayerMaskNames)
        {
            Position = position;
            Scale = scale;
            Rotation = rotation;

            Health = health;
            MinHealth = minHealth;
            MaxHealth = maxHealth;

            ColorR = colorR;
            ColorG = colorG;
            ColorB = colorB;
            ColorA = colorA;

            Velocity = velocity;

            RayCount = rayCount;
            Margin = margin;

            JumpCount = jumpCount;
            JumpLimit = jumpLimit;
            JumpHeight = jumpHeight;

            GravityModifier = gravityModifier;

            LeftLayerMaskNames = leftLayerMaskNames;
            RightLayerMaskNames = rightLayerMaskNames;
            DownLayerMaskNames = downLayerMaskNames;
            UpLayerMaskNames = upLayerMaskNames;
        }

        #endregion // Constructors

    }
}
