﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Muffin
{
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class Actor : MonoBehaviour, IDisposable, IHasModel<IActorModel>
    {

        #region Inspector fields

        [SerializeField] private int m_health = 10;
        [SerializeField] private int m_minHealth = 0;
        [SerializeField] private int m_maxHealth = 10;

        [SerializeField] private Vector3 m_velocity = Vector3.zero;

        [SerializeField] private float m_acceleration = 50f;
        [SerializeField] private float m_deceleration = 100f;

        [SerializeField] private int   m_jumpCount  = 0;
        [SerializeField] private int   m_jumpLimit  = 2;
        [SerializeField] private float m_jumpHeight = 2.5f;

        [SerializeField] private Vector3 m_gravityModifier = Vector3.one;

        [SerializeField] private int m_rayCount = 3;
        [SerializeField] private float m_margin = 0.01f;

        [SerializeField] private LayerMask m_leftLayerMask  = default;
        [SerializeField] private LayerMask m_rightLayerMask = default;
        [SerializeField] private LayerMask m_downLayerMask  = default;
        [SerializeField] private LayerMask m_upLayerMask    = default;

        [SerializeField] private bool m_isCollisionLeft  = false;
        [SerializeField] private bool m_isCollisionRight = false;
        [SerializeField] private bool m_isCollisionDown  = false;
        [SerializeField] private bool m_isCollisionUp    = false;

        [SerializeField] private bool m_wasCollisionLeft  = false;
        [SerializeField] private bool m_wasCollisionRight = false;
        [SerializeField] private bool m_wasCollisionDown  = false;
        [SerializeField] private bool m_wasCollisionUp    = false;

        [SerializeField] private Vector2 m_inputDirection = Vector2.zero;
        [SerializeField] private bool    m_isJumpQueued   = false;

        #endregion // Inspector fields



        #region Fields

        // Constructor params

        private IGameController m_gameController;
        private IActorModelFactory m_actorModelFactory;

        // Components

        private SpriteRenderer m_spriteRenderer;
        private BoxCollider2D m_boxCollider2D;

        #endregion // Fields



        #region Properties

        public Vector3 Position
        {
            get => transform.position;
            private set => transform.position = value;
        }
        public Vector3 Scale
        {
            get => transform.localScale;
            private set => transform.localScale = value;
        }
        public Vector3 Rotation
        {
            get => transform.eulerAngles;
            private set => transform.eulerAngles = value;
        }

        public int Health
        {
            get => m_health;
            private set => m_health = value;
        }
        public int MinHealth
        {
            get => m_minHealth;
            private set => m_minHealth = value;
        }
        public int MaxHealth
        {
            get => m_maxHealth;
            private set => m_maxHealth = value;
        }

        public Color Color
        {
            get => GetComponent<SpriteRenderer>().color;
            private set => GetComponent<SpriteRenderer>().color = value;
        }

        public Vector3 Velocity
        {
            get => m_velocity;
            private set => m_velocity = value;
        }

        public float Acceleration
        {
            get => m_acceleration;
            private set => m_acceleration = value;
        }
        public float Deceleration
        {
            get => m_deceleration;
            private set => m_deceleration = value;
        }

        public int JumpCount
        {
            get => m_jumpCount;
            private set => m_jumpCount = value;
        }
        public int JumpLimit
        {
            get => m_jumpLimit;
            private set => m_jumpLimit = value;
        }
        public float JumpHeight
        {
            get => m_jumpHeight;
            private set => m_jumpHeight = value;
        }

        public Vector3 GravityModifier
        {
            get => m_gravityModifier;
            private set => m_gravityModifier = value;
        }

        public int RayCount
        {
            get => m_rayCount;
            private set => m_rayCount = value;
        }
        public float Margin
        {
            get => m_margin;
            private set => m_margin = value;
        }

        public LayerMask LeftLayerMask
        {
            get => m_leftLayerMask;
            private set => m_leftLayerMask = value;
        }
        public LayerMask RightLayerMask
        {
            get => m_rightLayerMask;
            private set => m_rightLayerMask = value;
        }
        public LayerMask DownLayerMask
        {
            get => m_downLayerMask;
            private set => m_downLayerMask = value;
        }
        public LayerMask UpLayerMask
        {
            get => m_upLayerMask;
            private set => m_upLayerMask = value;
        }

        public bool IsCollisionLeft
        {
            get => m_isCollisionLeft;
            private set => m_isCollisionLeft = value;
        }
        public bool IsCollisionRight
        {
            get => m_isCollisionRight;
            private set => m_isCollisionRight = value;
        }
        public bool IsCollisionDown
        {
            get => m_isCollisionDown;
            private set => m_isCollisionDown = value;
        }
        public bool IsCollisionUp
        {
            get => m_isCollisionUp;
            private set => m_isCollisionUp = value;
        }

        public bool WasCollisionLeft
        {
            get => m_wasCollisionLeft;
            private set => m_wasCollisionLeft = value;
        }
        public bool WasCollisionRight
        {
            get => m_wasCollisionRight;
            private set => m_wasCollisionRight = value;
        }
        public bool WasCollisionDown
        {
            get => m_wasCollisionDown;
            private set => m_wasCollisionDown = value;
        }
        public bool WasCollisionUp
        {
            get => m_wasCollisionUp;
            private set => m_wasCollisionUp = value;
        }

        public Vector2 InputDirection
        {
            get => m_inputDirection;
            private set => m_inputDirection = value;
        }
        public bool IsJumpQueued
        {
            get => m_isJumpQueued;
            private set => m_isJumpQueued = value;
        }

        // Helper

        public int MovingDirectionX => Velocity.x == 0 ? 0 : Velocity.x < 0 ? -1 : 1;
        public int MovingDirectionY => Velocity.y == 0 ? 0 : Velocity.y < 0 ? -1 : 1;
        public int MovingDirectionZ => Velocity.z == 0 ? 0 : Velocity.z < 0 ? -1 : 1;
        public Vector3Int MovingDirection => new Vector3Int(MovingDirectionX, MovingDirectionY, MovingDirectionZ);

        public Vector2 Size => m_boxCollider2D.bounds.size;
        public float Width => Size.x;
        public float Height => Size.y;

        #endregion // Properties



        #region Zenject construct

        [Inject]
        private void Construct (IGameController gameController, IActorModelFactory actorModelFactory)
        {
            m_gameController = gameController;
            m_actorModelFactory = actorModelFactory;
        }

        #endregion // Zenject construct



        #region MonoBehaviour methods

        private void OnValidate ()
        {
            SetMinHealth(MinHealth);
            SetMaxHealth(MaxHealth);
            SetHealth(Health);
            SetAcceleration(Acceleration);
            SetDeceleration(Deceleration);
            SetJumpCount(JumpCount);
            SetJumpLimit(JumpLimit);
            SetJumpHeight(JumpHeight);
            SetRayCount(RayCount);
            SetMargin(Margin);
            SetInputDirection(InputDirection);
        }

        private void Awake ()
        {
            m_spriteRenderer = GetComponent<SpriteRenderer>();
            m_boxCollider2D = GetComponent<BoxCollider2D>();
        }

        #endregion // MonoBehaviour methods



        #region Setters

        public void SetPosition (float x, float y, float z) => Position = new Vector3(x, y, z);
        public void SetPosition (float x, float y) => SetPosition(x, y, Position.z);
        public void SetPosition (Vector3 position) => SetPosition(position.x, position.y, position.z);
        public void SetPosition (Vector2 position) => SetPosition(position.x, position.y, Position.z);
        public void SetPositionX (float x) => SetPosition(x, Position.y, Position.z);
        public void SetPositionY (float y) => SetPosition(Position.x, y, Position.z);
        public void SetPositionZ (float z) => SetPosition(Position.x, Position.y, z);

        public void TranslatePosition (float dx, float dy, float dz) => SetPosition(Position.x + dx, Position.y + dy, Position.z + dz);
        public void TranslatePosition (float dx, float dy) => TranslatePosition(dx, dy, 0f);
        public void TranslatePosition (Vector3 translation) => TranslatePosition(translation.x, translation.y, translation.z);
        public void TranslatePosition (Vector2 translation) => TranslatePosition(translation.x, translation.y, 0f);
        public void TranslatePositionX (float dx) => TranslatePosition(dx, 0f, 0f);
        public void TranslatePositionY (float dy) => TranslatePosition(0f, dy, 0f);
        public void TranslatePositionZ (float dz) => TranslatePosition(0f, 0f, dz);

        public void SetScale (float sx, float sy, float sz) => Scale = new Vector3(sx, sy, sz);
        public void SetScale (float sx, float sy) => SetScale(sx, sy, Scale.z);
        public void SetScale (Vector3 scale) => SetScale(scale.x, scale.y, scale.z);
        public void SetScale (Vector2 scale) => SetScale(scale.x, scale.y, Scale.z);
        public void SetScaleX (float sx) => SetScale(sx, Scale.y, Scale.z);
        public void SetScaleY (float sy) => SetScale(Scale.x, sy, Scale.z);
        public void SetScaleZ (float sz) => SetScale(Scale.x, Scale.y, sz);

        public void SetRotation (float rx, float ry, float rz) => Rotation = new Vector3(rx, ry, rz);
        public void SetRotation (Vector3 rotation) => SetRotation(rotation.x, rotation.y, rotation.z);
        public void SetRotationX (float rx) => SetRotation(rx, Rotation.y, Rotation.z);
        public void SetRotationY (float ry) => SetRotation(Rotation.x, ry, Rotation.z);
        public void SetRotationZ (float rz) => SetRotation(Rotation.x, Rotation.y, rz);

        public void SetHealth (int health)
        {
            Health = Mathf.Clamp(health, MinHealth, MaxHealth);
        }
        public void SetMinHealth (int minHealth)
        {
            MinHealth = minHealth;
            if (Health < MinHealth)
            {
                SetHealth(MinHealth);
            }
            if (MaxHealth < MinHealth)
            {
                SetMaxHealth(MinHealth);
            }
        }
        public void SetMaxHealth (int maxHealth)
        {
            MaxHealth = maxHealth;
            if (Health > MaxHealth)
            {
                SetHealth(MaxHealth);
            }
            if (MinHealth > MaxHealth)
            {
                SetMinHealth(MaxHealth);
            }
        }

        public void SetColor (float r, float g, float b, float a) => Color = new Color(r, g, b, a);
        public void SetColor (float r, float g, float b) => SetColor(r, g, b, Color.a);
        public void SetColor (Color color) => SetColor(color.r, color.g, color.b, color.a);
        public void SetColorR (float r) => SetColor(r, Color.g, Color.b, Color.a);
        public void SetColorG (float g) => SetColor(Color.r, g, Color.b, Color.a);
        public void SetColorB (float b) => SetColor(Color.r, Color.g, b, Color.a);
        public void SetColorA (float a) => SetColor(Color.r, Color.g, Color.b, a);

        public void SetVelocity (float vx, float vy, float vz) => Velocity = new Vector3(vx, vy, vz);
        public void SetVelocity (float vx, float vy) => SetVelocity(vx, vy, Velocity.z);
        public void SetVelocity (Vector3 velocity) => SetVelocity(velocity.x, velocity.y, velocity.z);
        public void SetVelocity (Vector2 velocity) => SetVelocity(velocity.x, velocity.y, Velocity.z);
        public void SetVelocityX (float vx) => SetVelocity(vx, Velocity.y, Velocity.z);
        public void SetVelocityY (float vy) => SetVelocity(Velocity.x, vy, Velocity.z);
        public void SetVelocityZ (float vz) => SetVelocity(Velocity.x, Velocity.y, vz);

        public void TranslateVelocity (float dvx, float dvy, float dvz) => SetVelocity(Velocity.x + dvx, Velocity.y + dvy, Velocity.z + dvz);
        public void TranslateVelocity (float dvx, float dvy) => TranslateVelocity(dvx, dvy, 0f);
        public void TranslateVelocity (Vector3 translation) => TranslateVelocity(translation.x, translation.y, translation.z);
        public void TranslateVelocity (Vector2 translation) => TranslateVelocity(translation.x, translation.y, 0f);
        public void TranslateVelocityX (float dvx) => TranslateVelocity(dvx, 0f, 0f);
        public void TranslateVelocityY (float dvy) => TranslateVelocity(0f, dvy, 0f);
        public void TranslateVelocityZ (float dvz) => TranslateVelocity(0f, 0f, dvz);

        public void SetAcceleration (float acceleration) => Acceleration = Mathf.Max(0f, acceleration);
        public void SetDeceleration (float deceleration) => Deceleration = Mathf.Max(0f, deceleration);

        public void SetJumpCount (int jumpCount) => JumpCount = Mathf.Max(0, jumpCount);
        public void IncreaseJumpCount () => SetJumpCount(JumpCount + 1);
        public void SetJumpLimit (int jumpLimit) => JumpLimit = Mathf.Max(0, jumpLimit);
        public void SetJumpHeight (float jumpHeight) => JumpHeight = Mathf.Max(0f, jumpHeight);

        public void SetGravityModifier (float gmx, float gmy, float gmz) => GravityModifier = new Vector3(gmx, gmy, gmz);
        public void SetGravityModifier (float gmx, float gmy) => SetGravityModifier(gmx, gmy, GravityModifier.z);
        public void SetGravityModifier (Vector3 gravityModifier) => SetGravityModifier(gravityModifier.x, gravityModifier.y, gravityModifier.z);
        public void SetGravityModifier (Vector2 gravityModifier) => SetGravityModifier(gravityModifier.x, gravityModifier.y, GravityModifier.z);
        public void SetGravityModifierX (float gmx) => SetGravityModifier(gmx, GravityModifier.y, GravityModifier.z);
        public void SetGravityModifierY (float gmy) => SetGravityModifier(GravityModifier.x, gmy, GravityModifier.z);
        public void SetGravityModifierZ (float gmz) => SetGravityModifier(GravityModifier.x, GravityModifier.y, gmz);

        public void SetRayCount (int rayCount) => RayCount = Mathf.Max(3, rayCount);
        public void SetMargin (float margin) => Margin = Mathf.Max(0f, margin);

        public void SetLeftLayerMask (LayerMask layerMask) => LeftLayerMask = layerMask;
        public void SetRightLayerMask (LayerMask layerMask) => RightLayerMask = layerMask;
        public void SetDownLayerMask (LayerMask layerMask) => DownLayerMask = layerMask;
        public void SetUpLayerMask (LayerMask layerMask) => UpLayerMask = layerMask;

        public void SetIsCollisionLeft (bool isCollision) => IsCollisionLeft = isCollision;
        public void SetIsCollisionRight (bool isCollision) => IsCollisionRight = isCollision;
        public void SetIsCollisionDown (bool isCollision) => IsCollisionDown = isCollision;
        public void SetIsCollisionUp (bool isCollision) => IsCollisionUp = isCollision;

        public void SetWasCollisionLeft (bool wasCollision) => WasCollisionLeft = wasCollision;
        public void SetWasCollisionRight (bool wasCollision) => WasCollisionRight = wasCollision;
        public void SetWasCollisionDown (bool wasCollision) => WasCollisionDown = wasCollision;
        public void SetWasCollisionUp (bool wasCollision) => WasCollisionUp = wasCollision;

        public void SetInputDirection (float x, float y)
        {
            InputDirection = new Vector2()
            {
                x = x < 0 ? -1 : x > 0 ? 1 : 0,
                y = y < 0 ? -1 : y > 0 ? 1 : 0
            };
        }
        public void SetInputDirection (Vector2 inputDirection) => SetInputDirection(inputDirection.x, inputDirection.y);
        public void SetInputDirectionX (float x) => SetInputDirection(x, InputDirection.y);
        public void SetInputDirectionY (float y) => SetInputDirection(InputDirection.x, y);

        public void SetIsJumpQueued (bool isJumpQueued) => IsJumpQueued = isJumpQueued;
        public void QueueJump () => SetIsJumpQueued(true);

        #endregion // Setters



        #region Public methods

        public void FixedTick (float dt)
        {
            HandleInput(dt);

            TranslateVelocityY(Physics2D.gravity.y * GravityModifier.y * (dt / 2f));

            HandleCollision(dt);

            if (!IsCollisionDown)
            {
                TranslateVelocityY(Physics2D.gravity.y * GravityModifier.y * (dt / 2f));
            }

            TranslatePosition(Velocity * dt);
        }
        public void Tick (float dt)
        {
            PollInput();
        }
        public void LateTick (float dt)
        {

        }

        public IActorModel ExportModel ()
        {
            string[] GetLayerNames (LayerMask layerMask)
            {
                List<string> strings = new List<string>();

                for (int i = 0; i < 32; i++)
                {
                    if (layerMask == (layerMask | (1 << i)))
                    {
                        strings.Add(LayerMask.LayerToName(i));
                    }
                }

                return strings.ToArray();
            }

            return m_actorModelFactory.Create(
                Position, Scale, Rotation,
                Health, MinHealth, MaxHealth,
                Color.r, Color.g, Color.b, Color.a,
                Velocity,
                Acceleration, Deceleration,
                RayCount, Margin,
                JumpCount, JumpLimit, JumpHeight,
                GravityModifier,
                GetLayerNames(LeftLayerMask), GetLayerNames(RightLayerMask),
                GetLayerNames(DownLayerMask), GetLayerNames(UpLayerMask));
        }
        public void ImportModel (IActorModel model)
        {
            SetPosition(model.Position);
            SetScale(model.Scale);
            SetRotation(model.Rotation);
            SetMinHealth(model.MinHealth);
            SetMaxHealth(model.MaxHealth);
            SetHealth(model.Health);
            SetColor(model.ColorR, model.ColorG, model.ColorB, model.ColorA);
            SetVelocity(model.Velocity);
            SetAcceleration(model.Acceleration);
            SetDeceleration(model.Deceleration);
            SetRayCount(model.RayCount);
            SetMargin(model.Margin);
            SetJumpCount(model.JumpCount);
            SetJumpLimit(model.JumpLimit);
            SetJumpHeight(model.JumpHeight);
            SetGravityModifier(model.GravityModifier);
            SetLeftLayerMask(LayerMask.GetMask(model.LeftLayerMaskNames));
            SetRightLayerMask(LayerMask.GetMask(model.RightLayerMaskNames));
            SetDownLayerMask(LayerMask.GetMask(model.DownLayerMaskNames));
            SetUpLayerMask(LayerMask.GetMask(model.UpLayerMaskNames));
        }

        public void Dispose ()
        {
            Destroy(gameObject);
        }

        #endregion // Public methods



        #region Private methods

        private void HandleInput (float dt)
        {
            if (InputDirection.x != 0)
            {
                TranslateVelocityX(Acceleration * InputDirection.x * dt);
            }
            else if (Velocity.x != 0)
            {
                int originalMovingDirectionX = MovingDirectionX;
                TranslateVelocityX(Deceleration * -originalMovingDirectionX * dt);

                if (originalMovingDirectionX != MovingDirectionX)
                {
                    SetVelocityX(0f);
                }
            }

            if (IsJumpQueued)
            {
                SetIsJumpQueued(false);

                if (JumpCount < JumpLimit)
                {
                    IncreaseJumpCount();
                    SetVelocityY(Mathf.Sqrt(-2 * (Physics2D.gravity.y * GravityModifier.y) * JumpHeight));
                }
            }
        }

        private void HandleCollision (float dt)
        {
            SetWasCollisionLeft(IsCollisionLeft);
            SetWasCollisionRight(IsCollisionRight);
            SetWasCollisionDown(IsCollisionDown);
            SetWasCollisionUp(IsCollisionUp);

            SetIsCollisionLeft(false);
            SetIsCollisionRight(false);
            SetIsCollisionDown(false);
            SetIsCollisionUp(false);

            if (Velocity.x != 0)
            {
                Vector2 start = new Vector2(m_boxCollider2D.bounds.center.x, m_boxCollider2D.bounds.min.y + Margin);
                Vector2 end = new Vector2(m_boxCollider2D.bounds.center.x, m_boxCollider2D.bounds.max.y - Margin);

                Vector2 direction = Velocity.x < 0 ? Vector2.left : Vector2.right;

                float _moveDistX = Mathf.Abs(Velocity.x) * dt;
                float _largestOfMoveDistXAndMargin = _moveDistX > Margin ? _moveDistX : Margin;
                float distance = m_boxCollider2D.bounds.extents.x + _largestOfMoveDistXAndMargin;

                LayerMask layerMask = direction == Vector2.left ? LeftLayerMask : RightLayerMask;

                List<RaycastHit2D> hits = new List<RaycastHit2D>();

                for (int i = 0; i < RayCount; i++)
                {
                    float lerp = (float)i / (RayCount - 1);
                    Vector2 origin = Vector2.Lerp(start, end, lerp);
                    RaycastHit2D hit = Physics2D.Raycast(origin, direction, distance, layerMask);
                    hits.Add(hit);
                }

                if (hits.Any(_hit => _hit.collider != null))
                {
                    int indexOfFirstShortestRay = -1;
                    float firstShortestDistance = Mathf.Infinity;

                    for (int i = 0; i < hits.Count; i++)
                    {
                        if (hits[i].collider != null && hits[i].distance < firstShortestDistance)
                        {
                            firstShortestDistance = hits[i].distance;
                            indexOfFirstShortestRay = i;
                        }
                    }

                    RaycastHit2D hit = hits[indexOfFirstShortestRay];

                    if (hit.normal.x == 1)
                    {
                        SetIsCollisionLeft(true);
                        SetVelocityX(0f);
                        SetPositionX(hit.point.x + m_boxCollider2D.bounds.extents.x);
                    }
                    else if (hit.normal.x == -1)
                    {
                        SetIsCollisionRight(true);
                        SetVelocityX(0f);
                        SetPositionX(hit.point.x - m_boxCollider2D.bounds.extents.x);
                    }
                }
            }

            if (Velocity.y != 0)
            {
                Vector2 start = new Vector2(m_boxCollider2D.bounds.min.x + Margin, m_boxCollider2D.bounds.center.y);
                Vector2 end = new Vector2(m_boxCollider2D.bounds.max.x - Margin, m_boxCollider2D.bounds.center.y);

                Vector2 direction = Velocity.y < 0 ? Vector2.down : Vector2.up;

                float _moveDistY = Mathf.Abs(Velocity.y) * dt;
                float _largestOfMoveDistYAndMargin = _moveDistY > Margin ? _moveDistY : Margin;
                float distance = m_boxCollider2D.bounds.extents.y + _largestOfMoveDistYAndMargin;

                LayerMask layerMask = direction == Vector2.down ? DownLayerMask : UpLayerMask;

                List<RaycastHit2D> hits = new List<RaycastHit2D>();

                for (int i = 0; i < RayCount; i++)
                {
                    float lerp = (float)i / (RayCount - 1);
                    Vector2 origin = Vector2.Lerp(start, end, lerp);
                    RaycastHit2D hit = Physics2D.Raycast(origin, direction, distance, layerMask);
                    hits.Add(hit);
                }

                if (hits.Any(_hit => _hit.collider != null))
                {
                    int indexOfFirstShortestRay = -1;
                    float firstShortestDistance = Mathf.Infinity;

                    for (int i = 0; i < hits.Count; i++)
                    {
                        if (hits[i].collider != null && hits[i].distance < firstShortestDistance)
                        {
                            firstShortestDistance = hits[i].distance;
                            indexOfFirstShortestRay = i;
                        }
                    }

                    RaycastHit2D hit = hits[indexOfFirstShortestRay];

                    if (hit.normal.y == 1)
                    {
                        SetIsCollisionDown(true);
                        SetJumpCount(0);
                        SetVelocityY(0f);
                        SetPositionY(hit.point.y + m_boxCollider2D.bounds.extents.y);
                    }
                    else if (hit.normal.y == -1)
                    {
                        SetIsCollisionUp(true);
                        SetVelocityY(0f);
                        SetPositionY(hit.point.y - m_boxCollider2D.bounds.extents.y);
                    }
                }
            }
        }

        private void PollInput ()
        {
            SetInputDirectionX(Input.GetAxisRaw("Horizontal"));
            SetInputDirectionY(Input.GetAxisRaw("Vertical"));

            if (Input.GetKeyDown(KeyCode.Space))
            {
                QueueJump();
            }
        }

        #endregion // Private methods

    }
}
