﻿using UnityEngine;

namespace Muffin
{
    public class ActorFactory : IActorFactory
    {

        #region Fields

        private readonly GameObject m_prefab;
        private readonly Transform m_parent;
        private readonly IActorModel m_defaultModel;

        #endregion // Fields



        #region Constructors

        public ActorFactory (GameObject prefab, Transform parent, IActorModel defaultModel)
        {
            m_prefab = prefab;
            m_parent = parent;
            m_defaultModel = defaultModel;
        }

        #endregion // Constructors



        #region Public methods

        public Actor Create ()
        {
            GameObject gameObject = UnityEngine.Object.Instantiate(m_prefab, m_parent);
            gameObject.name = "Actor";
            Actor actor = gameObject.GetComponent<Actor>();
            actor.ImportModel(m_defaultModel);

            return actor;
        }

        public Actor Create (Vector3 position)
        {
            Actor actor = Create();
            actor.SetPosition(position);

            return actor;
        }

        public Actor Create (IActorModel model)
        {
            Actor actor = Create();
            actor.ImportModel(model);

            return actor;
        }

        public Actor Create (Actor actor)
        {
            return Create(actor.ExportModel());
        }

        #endregion // Public methods

    }
}
