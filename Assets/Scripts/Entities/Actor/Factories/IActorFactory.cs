﻿using UnityEngine;

namespace Muffin
{
    public interface IActorFactory
    {
        Actor Create ();
        Actor Create (Vector3 position);
        Actor Create (IActorModel model);
        Actor Create (Actor actor);
    }
}