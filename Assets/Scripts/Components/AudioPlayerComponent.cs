﻿using UnityEngine;
using Zenject;

namespace Muffin
{
    public class AudioPlayerComponent : MonoBehaviour, IAudioPlayer
    {

        #region Inspector fields

        [SerializeField] private AudioSource m_soundAudioSource = default;
        [SerializeField] private AudioSource m_musicAudioSource = default;

        #endregion // Inspector fields



        #region Fields

        private IRepository<AudioClip> m_soundRepository;
        private IRepository<AudioClip> m_musicRepository;
        private ILogger m_logger;

        #endregion // Fields



        #region Zenject construct

        [Inject]
        private void Construct (
            IRepository<AudioClip> soundRepository,
            IRepository<AudioClip> musicRepository,
            ILogger logger)
        {
            m_soundRepository = soundRepository;
            m_musicRepository = musicRepository;
            m_logger = logger;
        }

        #endregion // Zenject construct



        #region MonoBehaviour methods

        private void OnDestroy ()
        {
            StopAll();
        }

        #endregion // MonoBehaviour methods



        #region Public methods

        public void PlaySound (int id, float volumeScale = 1f)
        {
            //if (m_soundRepository.Contains(id))
            //{
            //    AudioClip audioClip = m_soundRepository.Get(id);
            //    m_soundAudioSource.PlayOneShot(audioClip, volumeScale);
            //}
            //else
            //{
            //    m_logger.Log($"Warning: AudioPlayer -> Attempting to play sound - ID {id} does not exist.");
            //}
        }

        public void PlayMusic (int id)
        {
            //m_logger.Log($"Event: AudioPlayer -> Attempting to play music at ID '{id.ToString()}'.");

            //m_logger.Log($"Event: AudioPlayer -> Stopping current track.");
            //m_musicAudioSource.Stop();
            //m_logger.Log($"Event: AudioPlayer -> Current track stopped.");

            //m_logger.Log($"Event: AudioPlayer -> Checking audio repository for ID '{id.ToString()}'.");
            //if (m_musicRepository.Contains(id))
            //{
            //    m_logger.Log($"Event: AudioPlayer -> Audio repository contains ID '{id.ToString()}'.");
            //    m_logger.Log($"Event: AudioPlayer -> Attempting to retrieve audio clip from repository.");
            //    AudioClip audioClip = m_musicRepository.Get(id);
            //    if (audioClip != null)
            //    {
            //        m_logger.Log($"Event: AudioPlayer -> Successfully retrieved audio clip.");
            //        m_logger.Log($"Event: AudioPlayer -> Assigning audio clip.");
            //        m_musicAudioSource.clip = audioClip;
            //        m_logger.Log($"Event: AudioPlayer -> Playing audio clip.");
            //        m_musicAudioSource.Play();
            //    }
            //    else
            //    {
            //        m_logger.Log($"Warning: AudioPlayer -> Audio clip does not exist at ID '{id.ToString()}'.");
            //    }
            //}
            //else
            //{
            //    m_logger.Log($"Warning: AudioPlayer -> Audio clip does not exist at ID '{id.ToString()}'.");
            //}
        }

        public void StopSounds ()
        {
            m_logger.Log($"Event: AudioPlayer -> Stopping sounds.");
            m_soundAudioSource.Stop();
        }

        public void StopMusic ()
        {
            m_logger.Log($"Event: AudioPlayer -> Stopping music.");
            m_musicAudioSource.Stop();
        }

        public void StopAll ()
        {
            StopSounds();
            StopMusic();
        }

        #endregion // Public methods

    }
}
