﻿using UnityEngine;
using Zenject;

namespace Muffin
{
    public class App : IInitializable, IFixedTickable, ITickable, ILateTickable, ILateDisposable
    {

        #region Fields

        private readonly IGameController m_gameController;
        private readonly IActorFactory m_actorFactory;

        private readonly IRepository<IActorModel> m_actorModelRepository;

        #endregion // Fields



        #region Constructors

        public App (
            IGameController gameController,
            IActorFactory actorFactory,
            IRepository<IActorModel> actorModelRepository)
        {
            m_gameController = gameController;
            m_actorFactory = actorFactory;
            m_actorModelRepository = actorModelRepository;
        }

        #endregion // Constructors



        #region Public methods

        public void Initialize ()
        {
            m_gameController.Initialize();
        }

        public void FixedTick ()
        {
            m_gameController.FixedTick(Time.deltaTime);
        }

        public void Tick ()
        {
            m_gameController.Tick(Time.deltaTime);
        }

        public void LateTick ()
        {
            m_gameController.LateTick(Time.deltaTime);
        }

        public void LateDispose ()
        {
            m_gameController.Dispose();
        }

        #endregion // Public methods

    }
}
