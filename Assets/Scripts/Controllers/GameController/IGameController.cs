﻿using UnityEngine;

namespace Muffin
{
    public interface IGameController
    {
        void Initialize ();
        void Dispose ();

        void AddActor (Actor actor);
        void RemoveActor (Actor actor);
        void RemoveAllActors ();

        Actor CreateActor (Vector3 position = default);
        Actor CreateActor (IActorModel model);
        Actor CreateActor (IActorModel model, Vector3 position);
        Actor CreateActor (int id, Vector3 position = default);

        void AddForceSource (ForceSource forceSource);
        void RemoveForceSource (ForceSource forceSource);
        void RemoveAllForceSources ();

        ForceSource CreateForceSource (Vector3 position = default);
        ForceSource CreateForceSource (IForceSourceModel model);
        ForceSource CreateForceSource (IForceSourceModel model, Vector3 position);
        ForceSource CreateForceSource (int id, Vector3 position = default);

        void FixedTick (float dt);
        void Tick (float dt);
        void LateTick (float dt);
    }
}
