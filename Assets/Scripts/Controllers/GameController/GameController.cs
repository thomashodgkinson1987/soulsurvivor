﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;

namespace Muffin
{
    public class GameController : IGameController
    {

        #region Fields

        private readonly IList<Actor> m_actors;
        private readonly IList<ForceSource> m_forceSources;

        // Constructor fields

        private readonly ICameraController m_cameraController;

        private readonly IRepository<IActorModel> m_actorModelRepository;
        private readonly IRepository<IForceSourceModel> m_forceSourceModelRepository;

        private readonly IActorModelFactory m_actorModelFactory;
        private readonly IActorFactory m_actorFactory;

        private readonly IForceSourceModelFactory m_forceSourceModelFactory;
        private readonly IForceSourceFactory m_forceSourceFactory;

        private readonly Muffin.ILogger m_logger;

        #endregion // Fields



        #region Constructors

        public GameController (
            ICameraController cameraController,
            IRepository<IActorModel> actorModelRepository,
            IRepository<IForceSourceModel> forceSourceModelRepository,
            IActorModelFactory actorModelFactory,
            IActorFactory actorFactory,
            IForceSourceModelFactory forceSourceModelFactory,
            IForceSourceFactory forceSourceFactory,
            Muffin.ILogger logger)
        {
            m_cameraController = cameraController;

            m_actorModelRepository = actorModelRepository;
            m_forceSourceModelRepository = forceSourceModelRepository;

            m_actorModelFactory = actorModelFactory;
            m_actorFactory = actorFactory;

            m_forceSourceModelFactory = forceSourceModelFactory;
            m_forceSourceFactory = forceSourceFactory;

            m_logger = logger;

            m_actors = new List<Actor>();
            m_forceSources = new List<ForceSource>();
        }

        #endregion // Constructors



        #region Public methods

        public void Initialize ()
        {
            // Actor model repository populating
            {
                string filePath = Application.streamingAssetsPath;
                string fileName = "ActorModels.json";
                string json = File.ReadAllText(Path.Combine(filePath, fileName));
                IActorModel[] models = JsonConvert.DeserializeObject<ActorModel[]>(json);

                foreach (IActorModel model in models)
                {
                    m_actorModelRepository.Add(model);
                }
            }

            // Add current actors in scene
            {
                IList<Actor> actorsInScene = UnityEngine.Object.FindObjectsOfType<Actor>().ToList();
                for (int i = 0; i < actorsInScene.Count; i++)
                {
                    IActorModel model = actorsInScene[i].ExportModel();
                    Actor actor = CreateActor(model);
                    UnityEngine.Object.Destroy(actorsInScene[i].gameObject);
                }
            }

            // Add current force sources in scene
            {
                IList<ForceSource> forceSourcesInScene = UnityEngine.Object.FindObjectsOfType<ForceSource>().ToList();
                for (int i = 0; i < forceSourcesInScene.Count; i++)
                {
                    IForceSourceModel model = forceSourcesInScene[i].ExportModel();
                    ForceSource forceSource = CreateForceSource(model);
                    UnityEngine.Object.Destroy(forceSourcesInScene[i].gameObject);
                }
            }
        }
        public void Dispose ()
        {
            RemoveAllActors();
            RemoveAllForceSources();
        }

        public void FixedTick (float dt)
        {
            for (int i = 0; i < m_forceSources.Count; i++)
            {
                Vector3 position = m_forceSources[i].Position;
                float radius = m_forceSources[i].Radius;
                float sqrRadius = radius * radius;

                IEnumerable<Actor> actorsInRange = m_actors.Where(actor => (actor.Position - position).sqrMagnitude <= sqrRadius);

                foreach (Actor actor in actorsInRange)
                {
                    Vector2 displacement = actor.Position - position;
                    float alpha = 1f - (displacement.sqrMagnitude / sqrRadius);
                    float strength = Mathf.Lerp(0f, m_forceSources[i].Strength, alpha);
                    actor.TranslateVelocity(displacement.normalized * strength);
                }
            }

            for (int i = 0; i < m_actors.Count; i++)
            {
                m_actors[i].FixedTick(dt);
            }
        }
        public void Tick (float dt)
        {
            HandleDebugControls();

            for (int i = 0; i < m_actors.Count; i++)
            {
                m_actors[i].Tick(dt);
            }

            //UpdateCamera(dt);
        }
        public void LateTick (float dt)
        {
            for (int i = 0; i < m_actors.Count; i++)
            {
                m_actors[i].LateTick(dt);
            }
        }

        public Actor CreateActor (Vector3 position = default)
        {
            Actor actor = m_actorFactory.Create(position);
            m_actors.Add(actor);

            return actor;
        }
        public Actor CreateActor (IActorModel model)
        {
            Actor actor = CreateActor();
            actor.ImportModel(model);

            return actor;
        }
        public Actor CreateActor (IActorModel model, Vector3 position)
        {
            Actor actor = CreateActor(model);
            actor.SetPosition(position);

            return actor;
        }
        public Actor CreateActor (int id, Vector3 position = default)
        {
            IActorModel model = m_actorModelRepository.Get(id);
            Actor actor = CreateActor(model, position);

            return actor;
        }

        public void AddActor (Actor actor)
        {
            if (!m_actors.Contains(actor))
            {
                m_actors.Add(actor);
            }
        }
        public void RemoveActor (Actor actor)
        {
            actor.Dispose();
            m_actors.Remove(actor);
        }
        public void RemoveAllActors ()
        {
            for (int i = 0; i < m_actors.Count; i++)
            {
                m_actors[i].Dispose();
            }
            m_actors.Clear();
        }

        public ForceSource CreateForceSource (Vector3 position = default)
        {
            ForceSource forceSource = m_forceSourceFactory.Create(position);
            m_forceSources.Add(forceSource);

            return forceSource;
        }
        public ForceSource CreateForceSource (IForceSourceModel model)
        {
            ForceSource forceSource = CreateForceSource();
            forceSource.ImportModel(model);

            return forceSource;
        }
        public ForceSource CreateForceSource (IForceSourceModel model, Vector3 position)
        {
            ForceSource forceSource = CreateForceSource(model);
            forceSource.SetPosition(position);

            return forceSource;
        }
        public ForceSource CreateForceSource (int id, Vector3 position = default)
        {
            IForceSourceModel model = m_forceSourceModelRepository.Get(id);
            ForceSource forceSource = CreateForceSource(model, position);

            return forceSource;
        }

        public void AddForceSource (ForceSource forceSource)
        {
            if (!m_forceSources.Contains(forceSource))
            {
                m_forceSources.Add(forceSource);
            }
        }
        public void RemoveForceSource (ForceSource forceSource)
        {
            UnityEngine.Object.Destroy(forceSource.gameObject);
            m_forceSources.Remove(forceSource);
        }
        public void RemoveAllForceSources ()
        {
            for (int i = 0; i < m_forceSources.Count; i++)
            {
                UnityEngine.Object.Destroy(m_forceSources[i].gameObject);
            }
            m_forceSources.Clear();
        }

        #endregion // Public methods



        #region Private methods

        private void UpdateCamera (float dt)
        {
            if (m_actors.Count > 0)
            {
                Bounds bounds = new Bounds(m_actors[0].Position, m_actors[0].Size);
                for (int i = 0; i < m_actors.Count; i++)
                {
                    bounds.Encapsulate(m_actors[i].Position);
                }

                m_cameraController.SetTargetPosition(bounds.center.x, bounds.center.y);
            }
            else
            {
                m_cameraController.SetTargetPosition(0f, 0f);
            }

            m_cameraController.Tick(dt);
        }

        private void HandleDebugControls ()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                position.z = 0f;
                CreateActor(new Vector3(position.x, position.y, position.z));
            }

            if (Input.GetKeyDown(KeyCode.Q)) RemoveAllActors();

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                CreateActor(0, new Vector3(position.x, position.y, 0f));
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                CreateActor(1, new Vector3(position.x, position.y, 0f));
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                CreateActor(2, new Vector3(position.x, position.y, 0f));
            }
        }

        #endregion // Private methods

    }
}
