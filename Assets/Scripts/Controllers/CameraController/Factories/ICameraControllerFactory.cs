﻿using UnityEngine;

namespace Muffin
{
    public interface ICameraControllerFactory
    {
        ICameraController Create ();
        ICameraController Create (Vector3 position, Vector3 targetPosition, float lerpAlpha);
        ICameraController Create (ICameraControllerModel model);
    }
}