﻿using UnityEngine;

namespace Muffin
{
    public class CameraControllerFactory : ICameraControllerFactory
    {

        #region Fields

        private readonly Camera m_camera;
        private readonly ICameraControllerModelFactory m_cameraControllerModelFactory;

        #endregion // Fields



        #region Constructors

        public CameraControllerFactory (Camera camera, ICameraControllerModelFactory cameraControllerModelFactory)
        {
            m_camera = camera;
            m_cameraControllerModelFactory = cameraControllerModelFactory;
        }

        #endregion // Constructors



        #region Public methods

        public ICameraController Create ()
        {
            ICameraController controller = new CameraController(m_camera, m_cameraControllerModelFactory);

            return controller;
        }

        public ICameraController Create (Vector3 position, Vector3 targetPosition, float lerpAlpha)
        {
            ICameraController controller = Create();

            controller.SetPosition(position);
            controller.SetTargetPosition(targetPosition);
            controller.SetLerpAlpha(lerpAlpha);

            return controller;
        }

        public ICameraController Create (ICameraControllerModel model)
        {
            ICameraController controller = Create();
            controller.ImportModel(model);

            return controller;
        }

        #endregion // Public methods

    }
}
