﻿namespace Muffin
{
    public interface ICameraControllerModel
    {
        float PositionX { get; set; }
        float PositionY { get; set; }
        float PositionZ { get; set; }

        float TargetPositionX { get; set; }
        float TargetPositionY { get; set; }
        float TargetPositionZ { get; set; }

        float LerpAlpha { get; set; }
    }
}