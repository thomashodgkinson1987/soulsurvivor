﻿using System;

namespace Muffin
{
    [Serializable]
    public class CameraControllerModel : ICameraControllerModel
    {

        #region Fields

        public float PositionX { get; set; } = 0f;
        public float PositionY { get; set; } = 0f;
        public float PositionZ { get; set; } = -10f;

        public float TargetPositionX { get; set; } = 0f;
        public float TargetPositionY { get; set; } = 0f;
        public float TargetPositionZ { get; set; } = -10f;

        public float LerpAlpha { get; set; } = 0.1f;

        #endregion // Fields



        #region Constructors

        public CameraControllerModel () { }

        public CameraControllerModel (
            float positionX, float positionY, float positionZ,
            float targetPositionX, float targetPositionY, float targetPositionZ,
            float lerpAlpha)
        {
            PositionX = positionX;
            PositionY = positionY;
            PositionZ = positionZ;

            TargetPositionX = targetPositionX;
            TargetPositionY = targetPositionY;
            TargetPositionZ = targetPositionZ;

            LerpAlpha = lerpAlpha;
        }

        #endregion // Constructors

    }
}
