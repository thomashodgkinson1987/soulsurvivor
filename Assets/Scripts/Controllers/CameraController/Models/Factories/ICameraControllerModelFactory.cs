﻿using UnityEngine;

namespace Muffin
{
    public interface ICameraControllerModelFactory
    {
        ICameraControllerModel Create ();
        ICameraControllerModel Create (Vector3 position, Vector3 targetPosition, float lerpAlpha);
        ICameraControllerModel Create (
            float positionX, float positionY, float positionZ,
            float targetPositionX, float targetPositionY, float targetPositionZ,
            float lerpAlpha);
    }
}