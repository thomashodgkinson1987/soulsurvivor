﻿using UnityEngine;

namespace Muffin
{
    public class CameraControllerModelFactory : ICameraControllerModelFactory
    {

        #region Constructors

        public CameraControllerModelFactory () { }

        #endregion // Constructors



        #region Public methods

        public ICameraControllerModel Create ()
        {
            return new CameraControllerModel();
        }

        public ICameraControllerModel Create (Vector3 position, Vector3 targetPosition, float lerpAlpha)
        {
            return new CameraControllerModel(
                position.x, position.y, position.z,
                targetPosition.x, targetPosition.y, targetPosition.z,
                lerpAlpha);
        }

        public ICameraControllerModel Create (
            float positionX, float positionY, float positionZ,
            float targetPositionX, float targetPositionY, float targetPositionZ,
            float lerpAlpha)
        {
            return new CameraControllerModel(
                positionX, positionY, positionZ,
                targetPositionX, targetPositionY, targetPositionZ,
                lerpAlpha);
        }

        #endregion // Public methods

    }
}
