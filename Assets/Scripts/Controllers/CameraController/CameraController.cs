﻿using UnityEngine;

namespace Muffin
{
    public class CameraController : ICameraController
    {

        #region Fields

        private readonly Camera m_camera;
        private readonly ICameraControllerModelFactory m_cameraControllerModelFactory;

        #endregion // Fields



        #region Properties

        public Vector3 Position
        {
            get => m_camera.transform.position;
            private set => m_camera.transform.position = value;
        }
        public Vector3 TargetPosition { get; private set; }
        public float LerpAlpha { get; private set; }

        #endregion // Properties



        #region Constructors

        public CameraController (Camera camera, ICameraControllerModelFactory cameraControllerModelFactory)
        {
            m_camera = camera;
            m_cameraControllerModelFactory = cameraControllerModelFactory;

            TargetPosition = Position;
            LerpAlpha = 0f;
        }

        #endregion // Constructors



        #region Setters

        public void SetPosition (float x, float y, float z) => Position = new Vector3(x, y, z);
        public void SetPosition (float x, float y) => SetPosition(x, y, Position.z);
        public void SetPosition (Vector3 position) => SetPosition(position.x, position.y, position.z);
        public void SetPosition (Vector2 position) => SetPosition(position.x, position.y, Position.z);
        public void SetPositionX (float x) => SetPosition(x, Position.y, Position.z);
        public void SetPositionY (float y) => SetPosition(Position.x, y, Position.z);
        public void SetPositionZ (float z) => SetPosition(Position.x, Position.y, z);

        public void TranslatePosition (float dx, float dy, float dz) => SetPosition(Position + new Vector3(dx, dy, dz));
        public void TranslatePosition (float dx, float dy) => TranslatePosition(dx, dy, 0f);
        public void TranslatePosition (Vector3 translation) => TranslatePosition(translation.x, translation.y, translation.z);
        public void TranslatePosition (Vector2 translation) => TranslatePosition(translation.x, translation.y, 0f);
        public void TranslatePositionX (float dx) => TranslatePosition(dx, 0f, 0f);
        public void TranslatePositionY (float dy) => TranslatePosition(0f, dy, 0f);
        public void TranslatePositionZ (float dz) => TranslatePosition(0f, 0f, dz);

        public void SetTargetPosition (float x, float y, float z) => TargetPosition = new Vector3(x, y, z);
        public void SetTargetPosition (float x, float y) => SetTargetPosition(x, y, TargetPosition.z);
        public void SetTargetPosition (Vector3 targetPosition) => SetTargetPosition(targetPosition.x, targetPosition.y, targetPosition.z);
        public void SetTargetPosition (Vector2 targetPosition) => SetTargetPosition(targetPosition.x, targetPosition.y, TargetPosition.z);
        public void SetTargetPositionX (float x) => SetTargetPosition(x, TargetPosition.y, TargetPosition.z);
        public void SetTargetPositionY (float y) => SetTargetPosition(TargetPosition.x, y, TargetPosition.z);
        public void SetTargetPositionZ (float z) => SetTargetPosition(TargetPosition.x, TargetPosition.y, z);

        public void SetLerpAlpha (float lerpAlpha) => LerpAlpha = Mathf.Max(0f, lerpAlpha);

        #endregion // Setters



        #region Public methods

        public void Tick (float dt)
        {
            if (Position != TargetPosition)
            {
                if (LerpAlpha > 0)
                {
                    Vector3 nextPosition = Vector3.Lerp(Position, TargetPosition, LerpAlpha);
                    Vector3 newPosition = Vector3.Distance(Position, TargetPosition) > Vector3.Distance(Position, nextPosition) ? nextPosition : TargetPosition;
                    SetPosition(newPosition);
                }
                else
                {
                    SetPosition(TargetPosition);
                }
            }
        }

        public ICameraControllerModel ExportModel ()
        {
            return m_cameraControllerModelFactory.Create(
                Position.x, Position.y, Position.z,
                TargetPosition.x, TargetPosition.y, TargetPosition.z,
                LerpAlpha);
        }

        public void ImportModel (ICameraControllerModel model)
        {
            SetPosition(model.PositionX, model.PositionY, model.PositionZ);
            SetTargetPosition(model.TargetPositionX, model.TargetPositionY, model.TargetPositionZ);
            SetLerpAlpha(model.LerpAlpha);
        }

        public void Dispose () { }

        #endregion // Public methods

    }
}
