﻿using System;
using UnityEngine;

namespace Muffin
{
    public interface ICameraController : IDisposable
    {

        #region Properties

        Vector3 Position { get; }
        Vector3 TargetPosition { get; }
        float LerpAlpha { get; }

        #endregion // Properties



        #region Setters

        void SetPosition (float x, float y, float z);
        void SetPosition (float x, float y);
        void SetPosition (Vector3 position);
        void SetPosition (Vector2 position);
        void SetPositionX (float x);
        void SetPositionY (float y);
        void SetPositionZ (float z);

        void TranslatePosition (float dx, float dy, float dz);
        void TranslatePosition (float dx, float dy);
        void TranslatePosition (Vector3 translation);
        void TranslatePosition (Vector2 translation);
        void TranslatePositionX (float dx);
        void TranslatePositionY (float dy);
        void TranslatePositionZ (float dz);

        void SetTargetPosition (float x, float y, float z);
        void SetTargetPosition (float x, float y);
        void SetTargetPosition (Vector3 targetPosition);
        void SetTargetPosition (Vector2 targetPosition);
        void SetTargetPositionX (float x);
        void SetTargetPositionY (float y);
        void SetTargetPositionZ (float z);

        void SetLerpAlpha (float lerpAlpha);

        #endregion // Setters



        #region Methods

        void Tick (float dt);

        void ImportModel (ICameraControllerModel model);
        ICameraControllerModel ExportModel ();

        #endregion // Methods

    }
}
